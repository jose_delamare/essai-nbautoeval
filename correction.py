# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 10:33:11 2020

@author: José
"""

# importing the ExerciseFunction class
from nbautoeval import ExerciseFunction, Args
from nbautoeval import CallRenderer, PPrintRenderer



########## step 1
# You need to define the 'correct' function
# i.e. one that would be accepted as valid for the problem

# @BEG@ name=somme_double_somme
def somme_double_somme(n1, n2):
    somme = n1 + n2
    double_somme = 2*(n1 + n2)    
    return somme, double_somme
# @END@
    
inputs_somme_double_somme = [
    	Args(15,20),
	Args(15,0),
	Args(-15,20),
	Args(-15,-20),
]

########## step 3
# finally we create the exercise object
# NOTE: this is the only name that should be imported from this module
#
exo_somme_double_somme = ExerciseFunction(
    # first argument is the 'correct' function
    # it is recommended to use the same name as in the notebook, as the
    # python function name is used in HTML rendering 
    somme_double_somme,
    # the inputs
    inputs_somme_double_somme,
    result_renderer=PPrintRenderer(width=20),
)
